package test.demo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import test.demo.exception.ConfigurationNotFoundException;
import test.demo.handlers.HTTPHandler;
import test.demo.model.*;
import test.demo.model.request.HttpRequest.RequestHTTP;
import test.demo.model.request.Request;
import test.demo.repository.ConfigurationRepository;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/tests")
public class MyController {
	ConfigurationRepository repository;

	public MyController(ConfigurationRepository configurationRepository) {
		repository = configurationRepository;
	}

//	please check for uniqueness of name
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping()
	public Configuration create(@RequestBody Configuration stream)  {
		return repository.create((Configuration) stream);
	}

	@GetMapping()
	public List<Configuration> findAll() {
		return repository.findAll();
	}

	@GetMapping("/{id}")
	public Configuration findById(@PathVariable String id) throws ConfigurationNotFoundException {
		return repository.findById(id);
	}

	@ResponseStatus(HttpStatus.NO_CONTENT)
	@PutMapping("/{id}")
	public void update(@RequestBody Configuration stream, @PathVariable String id) {
		repository.update(stream,id);
	}

	@ResponseStatus(HttpStatus.NO_CONTENT)
	@DeleteMapping("/{id}")
	public void delete(@PathVariable String id) {
		repository.delete(id);
	}

	@ResponseStatus(HttpStatus.CREATED)
	@PutMapping("/serve")
	public void serve(@RequestBody Request req) throws ConfigurationNotFoundException, IOException {

		System.out.println(req.getClass());
		System.out.println(req.getCategory());
		System.out.println(req.getConfigId());

		if (Objects.equals(req.getCategory(), "HTTP")) {
			HTTPHandler.handle((RequestHTTP) req, findById(req.getConfigId()));
		}
	}





//	@GetMapping
//	public Temp myveryfun(HttpServletRequest req) {
//
//		Temp t = new Temp();
//		if (req.getParameter("name") != null) t.a = Integer.parseInt(req.getParameter("name"));
//		if (req.getParameter("age") != null) t.b = Integer.parseInt(req.getParameter("age"));
//
//		return t;
//	}

	@GetMapping("/{a}/{b}")
	public Temp mynewfun(@PathVariable int a, @PathVariable int b) {
		Temp t = new Temp();
		t.a = a;
		t.b = b;
		return t;
	}

//	@GetMapping("/{handle}")
//	public List<Contests> cffun(@PathVariable String handle) throws IOException {
//		URL urlForGetRequest = new URL("https://codeforces.com/api/user.rating?handle=" + handle);
//		String readLine = null;
//		HttpURLConnection connection = (HttpURLConnection) urlForGetRequest.openConnection();
//		connection.setRequestMethod("GET");
//
//		int responseCode = connection.getResponseCode();
//
//		System.out.println(responseCode);
//
//
////		if (responseCode == HttpURLConnection.HTTP_OK) {
//			BufferedReader in = new BufferedReader(
//					new InputStreamReader(connection.getInputStream()));
//			StringBuffer response = new StringBuffer();
//			while ((readLine = in.readLine()) != null) {
//				response.append(readLine);
//			}
//			in.close();
//			// print result
//			System.out.println("JSON String Result " + response.toString());
//			GsonBuilder builder = new GsonBuilder();
//			builder.setPrettyPrinting();
//
//			Gson gson = builder.create();
//			CFReturn data = gson.fromJson(response.toString(), CFReturn.class);
//			return data.getResult();
//			//GetAndPost.POSTRequest(response.toString());
////		} else {
////			System.out.println("GET NOT WORKED");
////			throw new IOException();
////		}
//	}
}
