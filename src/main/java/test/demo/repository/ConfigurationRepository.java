package test.demo.repository;

import org.springframework.stereotype.Repository;
import test.demo.exception.ConfigurationNotFoundException;
import test.demo.model.Configuration;
import test.demo.model.request.HttpRequest.RequestType;
import test.demo.model.variables.Variable;
import test.demo.model.variables.VariableType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
public class ConfigurationRepository {
	private static final List<Configuration> configurations = new ArrayList<>();

	static {
		Configuration tmp  = new Configuration();
		tmp.setIp("https://codeforces.com");
		tmp.setEndpoint("api/user.rating");
		tmp.setCommandName("User Rating");
		tmp.setRequestType(RequestType.GET);
		tmp.setVariables(Arrays.asList(new Variable("handle", VariableType.PATH_PARAM)));
		configurations.add(tmp);
	}

	public List<Configuration> findAll() {
		return configurations;
	}

	public Configuration findById(String id) throws ConfigurationNotFoundException {
		return configurations.stream().filter(stream -> stream.getId().equals(id)).findFirst().orElseThrow(ConfigurationNotFoundException::new);
	}

	public Configuration create(Configuration stream) {
		configurations.add(stream);
		return stream;
	}

	public void update(Configuration stream, String id) {
		Configuration existing = configurations.stream().filter(s -> s.getId().equals(id))
				.findFirst()
				.orElseThrow(() -> new IllegalArgumentException("Configuration not found"));
		int i = configurations.indexOf(existing);
		configurations.set(i, stream);
	}

	public void delete(String id) {
		configurations.removeIf(stream -> stream.getId().equals(id));
	}
}
