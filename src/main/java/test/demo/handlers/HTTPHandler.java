package test.demo.handlers;

import test.demo.model.Configuration;
import test.demo.model.request.HttpRequest.RequestHTTP;
import test.demo.model.request.HttpRequest.RequestType;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Map;

// check for service annotation
public class HTTPHandler {
	public static void handle(RequestHTTP req, Configuration config) throws IOException {
		StringBuilder urlSrt = new StringBuilder(config.getIp() + "/" + config.getEndpoint());

//		ASSUMING that all the optional parameters will be having something if it is present in this array
//		maybe while taking the input it is important to check that no extra "/" are there
		if (req.getPathParams() != null) {
			for (Map.Entry<String, String> entry : req.getPathParams().entrySet()) {
				urlSrt.append("/");
				urlSrt.append(entry.getValue());
			}
		}

		if (req.getQueryParams() != null) {
			if (!req.getQueryParams().isEmpty()) urlSrt.append("?");
			for (Map.Entry<String, String> entry : req.getQueryParams().entrySet()) {
				urlSrt.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
			}
			if (!req.getQueryParams().isEmpty()) urlSrt.deleteCharAt(urlSrt.length()-1);
		}

		System.out.println(urlSrt);

		URL url = new URL(urlSrt.toString());
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod(config.getRequestType().toString());


		if (config.getRequestType() == RequestType.POST) {
			OutputStream os = connection.getOutputStream();
			BufferedWriter writer = new BufferedWriter(
					new OutputStreamWriter(os, StandardCharsets.UTF_8));
			writer.write(req.getReqBody());

			writer.flush();
			writer.close();
			os.close();
		}

		int responseCode = connection.getResponseCode();

		if (responseCode == HttpsURLConnection.HTTP_OK) {
			String line;
			BufferedReader br=new BufferedReader(new InputStreamReader(connection.getInputStream()));
			while ((line=br.readLine()) != null) {
				System.out.println(line);
			}
		}
		else  {
			System.out.println("error");
		}
	}
}
