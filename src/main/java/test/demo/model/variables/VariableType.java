package test.demo.model.variables;

public enum VariableType {
	BODY_PARAM,
	QUERY_PARAM,
	PATH_PARAM
}
