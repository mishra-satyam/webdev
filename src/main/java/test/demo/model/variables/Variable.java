package test.demo.model.variables;

public class Variable {
	private String name;
	private VariableType type;

//	optional or not
//	default value
//	data type

	public Variable(String name, VariableType t)  {
		this.name = name;
		this.type =  t;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public VariableType getType() {
		return type;
	}

	public void setType(VariableType type) {
		this.type = type;
	}
}
