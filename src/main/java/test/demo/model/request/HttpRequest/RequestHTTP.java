package test.demo.model.request.HttpRequest;

import test.demo.model.request.Request;

import java.util.Map;

public class RequestHTTP extends Request {
	private final String reqBody;
	private final Map<String, String> queryParams;
	private final Map<String, String> pathParams;

	public RequestHTTP(String category, String configId, String reqBody, Map<String, String> queryParams, Map<String, String> pathParams) {
		super(category, configId);
		this.reqBody = reqBody;
		this.queryParams = queryParams;
		this.pathParams = pathParams;
	}

	public String getReqBody() {
		return reqBody;
	}

	public Map<String, String> getQueryParams() {
		return queryParams;
	}

	public Map<String, String> getPathParams() {
		return pathParams;
	}
}
