package test.demo.model.request.HttpRequest;

public enum RequestType {
	GET,
	POST,
	DELETE
}
