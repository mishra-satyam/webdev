package test.demo.model.request;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import test.demo.model.request.HttpRequest.RequestHTTP;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "category", visible = true)
@JsonSubTypes({
		@JsonSubTypes.Type(value = RequestHTTP.class, name = "HTTP")
})
public class Request {
	private final String category;

	private final String configId;

	public Request(String category, String configId) {
		this.category = category;
		this.configId = configId;
	}

	public String getCategory() {
		return category;
	}

	public String getConfigId() {
		return configId;
	}
}
