package test.demo.model;

import test.demo.model.request.HttpRequest.RequestType;
import test.demo.model.variables.Variable;

import java.util.List;

public class Configuration {
	private final String id;
	private String endpoint;
	private String ip;
	private RequestType requestType;
	private String commandName;

	private List<Variable> variables;

	public Configuration () {
		System.out.println("going to initialise id");
//		id = UUID.randomUUID().toString();
		id = "1";
	}

	public String getId() {
		return id;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public RequestType getRequestType() {
		return requestType;
	}

	public void setRequestType(RequestType requestType) {
		this.requestType = requestType;
	}

	public String getCommandName() {
		return commandName;
	}

	public void setCommandName(String commandName) {
		this.commandName = commandName;
	}

	public List<Variable> getVariables() {
		return variables;
	}

	public void setVariables(List<Variable> variables) {
		this.variables = variables;
	}
}
