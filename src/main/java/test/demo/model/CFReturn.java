package test.demo.model;

import java.util.List;

public class CFReturn {
	private String status;
	private String comment;
	private List<Contests> result;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public List<Contests> getResult() {
		return result;
	}

	public void setResult(List<Contests> result) {
		this.result = result;
	}

	public int getHighestRating() {
		int mx = 0;
		for (Contests c :
				result) {
			mx = Math.max(mx, c.getNewRating());
		}
		return mx;
	}

	public int bestRank() {
		int mn = Integer.MAX_VALUE;
		for (Contests c :
				result) {
			mn = Math.min(mn, c.getRank());
		}
		return mn;
	}
}
